<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{

    public function setUp()
    {
        parent::setUp();
        Artisan::call('db:seed', ['--class' => 'BlogTableSeeder']);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    /**
     * Test seed data
     */
    public function testFirstBlogData()
    {
        $count = App\Model\Blog::count();
        self::assertTrue($count === 1);
    }

    /**
     * Test see home page
     */
    public function testBasicExample()
    {
        $this->visit('/')
            ->see('CodePipeline Deploy Succeeded !!');
    }

}
